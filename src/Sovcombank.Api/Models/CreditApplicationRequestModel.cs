﻿using System;
using System.Net;
using Yes.Infrastructure.Common.Models;

namespace Sovcombank.Api.Models
{
    public class CreditApplicationRequestModel : JsonModel
    {
        /// <summary>
        /// Дата решения по заявке на кредит
        /// </summary>
        public DateTime CreatedDate { get; set; }
        
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
        
        /// <summary>
        /// Http статус запроса
        /// </summary>
        public HttpStatusCode HttpStatusCode { get; set; }
        
        /// <summary>
        /// Текст ошибки
        /// </summary>
        public string ErrorMessage { get; set; }
        
        /// <summary>
        /// Статус запроса
        /// </summary>
        public string SovcombankStatus { get; set; }
        
        /// <summary>
        /// Идентификатор запроса
        /// </summary>
        public int? SovcombankId { get; set; }
        
        /// <summary>
        /// </summary>
        public int? SovcombankRandom { get; set; }
        
        /// <summary>
        /// Причина ошибки
        /// </summary>
        public string SovcombankReason { get; set; }
    }
}