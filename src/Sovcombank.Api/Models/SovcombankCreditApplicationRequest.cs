﻿using Yes.Infrastructure.Common.Models;
// ReSharper disable InconsistentNaming

namespace Sovcombank.Api.Models
{
    public class SovcombankCreditApplicationRequest : JsonModel
    {
        public string api_key { get; set; }
        public int part_id { get; set; }
        public string product_name { get; set; }
        public string utm_medium { get; set; }
        public string utm_source { get; set; }
        public string utm_campaign { get; set; }
        public string utm_term { get; set; }
        public string utm_content { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public string parentName { get; set; }
        public int? birthDay { get; set; }
        public int? birthMonth { get; set; }
        public int? birthYear { get; set; }
        public string region { get; set; }
        public string mainPhone { get; set; }
        public string secondaryPhone { get; set; }
        public string email { get; set; }
        public string personal_data_processing_permission_status { get; set; }
        public string personal_data_agent { get; set; }
        public string personal_data_user { get; set; }
        public string personal_data_grant_date { get; set; }
    }
}