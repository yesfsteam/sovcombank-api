﻿using System;

namespace Sovcombank.Api.Models.Configuration
{
    public class ApplicationConfiguration
    {
        public Guid CreditOrganizationId { get; set; }
        public string ApiKey { get; set; }
        public int PartId { get; set; }
    }
}