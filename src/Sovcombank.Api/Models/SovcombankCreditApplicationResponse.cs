﻿using Newtonsoft.Json.Linq;
using Yes.Infrastructure.Common.Models;
// ReSharper disable InconsistentNaming

namespace Sovcombank.Api.Models
{
    public class SovcombankCreditApplicationResponse : JsonModel
    {
        public string status { get; set; }
        public int? id { get; set; }
        public int? random { get; set; }
        public JToken reason { get; set; }
    }
}