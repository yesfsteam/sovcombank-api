﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SD.Cqrs;
using SD.Messaging.Models;
using Sovcombank.Api.Data;
using Sovcombank.Api.Extensions;
using Sovcombank.Api.Models;
using Sovcombank.Api.Models.Configuration;
using Yes.Contracts;
using Yes.CreditApplication.Api.Contracts;
using Yes.CreditApplication.Api.Contracts.Commands;
using Yes.CreditApplication.Api.Contracts.CreditOrganizations;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.CreditApplication.Api.Contracts.Events;
using Yes.Infrastructure.Common.Extensions;
using Yes.Infrastructure.Http;
using CreditApplicationRequest = Yes.CreditApplication.Api.Contracts.CreditOrganizations.CreditApplicationRequest;

namespace Sovcombank.Api.Domain
{
    public interface ICreditApplicationManager
    {
        Task<ProcessingResult> CheckCreditApplication(CreditApplicationCreatedEvent @event);
        Task<Response<CreditApplicationResponse>> ConfirmCreditApplication(Guid creditApplicationId, CreditApplicationRequest model);
    }

    public class CreditApplicationManager : ICreditApplicationManager
    {
        private readonly ILogger<CreditApplicationManager> logger;
        private readonly ICreditApplicationRepository repository;
        private readonly ICommandSender commandSender;
        private readonly ISovcombankClient client;
        private readonly ApplicationConfiguration configuration;
        private readonly MessagingConfiguration messagingConfiguration;
        private const string SERVER_UNEXPECTED_ERROR_DETAILS = "Детали ошибки в логах приложения";
        private const string YESFS_URL = "yesfs.com";
        private readonly Dictionary<string, string> regions = new Dictionary<string, string>
        {
            {"2200000000000", "Алтайский край"},
            {"2800000000000", "Амурская область"},
            {"2900000000000", "Архангельская область"},
            {"3000000000000", "Астраханская область"},
            {"3100000000000", "Белгородская область"},
            {"3200000000000", "Брянская область"},
            {"3300000000000", "Владимирская область"},
            {"3400000000000", "Волгоградская область"},
            {"3500000000000", "Вологодская область"},
            {"3600000000000", "Воронежская область"},
            {"7900000000000", "Еврейская автономная область"},
            {"7500000000000", "Забайкальский край"},
            {"3700000000000", "Ивановская область"},
            {"3800000000000", "Иркутская область"},
            {"4000000000000", "Калужская область"},
            {"4100000000000", "Камчатский край"},
            {"0900000000000", "Карачаево-Черкесская Республика"},
            {"4200000000000", "Кемеровская область"},
            {"4300000000000", "Кировская область"},
            {"4400000000000", "Костромская область"},
            {"2300000000000", "Краснодарский край"},
            {"2400000000000", "Красноярский край"},
            {"4500000000000", "Курганская область"},
            {"4600000000000", "Курская область"},
            {"4800000000000", "Липецкая область"},
            {"4900000000000", "Магаданская область"},
            {"5000000000000", "Москва и Московская область"},
            {"7700000000000", "Москва и Московская область"},
            {"5100000000000", "Мурманская область"},
            {"8300000000000", "Ненецкий автономный округ"},
            {"5200000000000", "Нижегородская область"},
            {"5300000000000", "Новгородская область"},
            {"5400000000000", "Новосибирская область"},
            {"5500000000000", "Омская область"},
            {"5600000000000", "Оренбургская область"},
            {"5700000000000", "Орловская область"},
            {"5900000000000", "Пермский край"},
            {"5800000000000", "Пензенская область"},
            {"2500000000000", "Приморский край"},
            {"6000000000000", "Псковская область"},
            {"0100000000000", "Республика Адыгея"},
            {"0400000000000", "Республика Алтай"},
            {"0200000000000", "Республика Башкортостан"},
            {"0300000000000", "Республика Бурятия"},
            {"0800000000000", "Республика Калмыкия"},
            {"1000000000000", "Республика Карелия"},
            {"1100000000000", "Республика Коми"},
            {"1200000000000", "Республика Марий Эл"},
            {"1300000000000", "Республика Мордовия"},
            {"1400000000000", "Республика Саха(Якутия)"},
            {"1600000000000", "Республика Татарстан"},
            {"1900000000000", "Республика Хакасия"},
            {"6100000000000", "Ростовская область"},
            {"6200000000000", "Рязанская область"},
            {"6300000000000", "Самарская область"},
            {"4700000000000", "Санкт-Петербург и Ленинградская область"},
            {"7800000000000", "Санкт-Петербург и Ленинградская область"},
            {"6400000000000", "Саратовская область"},
            {"6500000000000", "Сахалинская область"},
            {"6600000000000", "Свердловская область"},
            {"2600000000000", "Ставропольский край"},
            {"6800000000000", "Тамбовская область"},
            {"6900000000000", "Тверская область"},
            {"7000000000000", "Томская область"},
            {"7100000000000", "Тульская область"},
            {"7200000000000", "Тюменская область"},
            {"1800000000000", "Удмуртская Республика"},
            {"7300000000000", "Ульяновская область"},
            {"2700000000000", "Хабаровский край"},
            {"8600000000000", "Ханты-Мансийский округ"},
            {"7400000000000", "Челябинская область"},
            {"2100000000000", "Чувашская Республика"},
            {"8700000000000", "Чукотский автономный округ"},
            {"7600000000000", "Ярославская область"},
            {"8900000000000", "Ямало-Ненецкий автономный округ"},
            {"3900000000000", "Калининградская область"},
            {"6700000000000", "Смоленская область"}
        };
        
        public CreditApplicationManager(ILogger<CreditApplicationManager> logger, ICreditApplicationRepository repository, 
            ICommandSender commandSender, ISovcombankClient client, ApplicationConfiguration configuration,
            MessagingConfiguration messagingConfiguration)
        {
            this.logger = logger;
            this.repository = repository;
            this.commandSender = commandSender;
            this.client = client;
            this.configuration = configuration;
            this.messagingConfiguration = messagingConfiguration;
        }

        public async Task<ProcessingResult> CheckCreditApplication(CreditApplicationCreatedEvent @event)
        {
            var beginTime = DateTime.Now;
            try
            {
                if (@event.ProfileType == ProfileType.Full || @event.ProfileType == ProfileType.Medium)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.NotApplicable, @event.CreditApplicationId, "Заявка автоматически не отправляется для полной и средней анкеты");
                    logger.LogInformation($"CheckCreditApplication. Skip. Unsupported profile type. Duration: {beginTime.GetDuration()} Event: {@event}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                string region;
                if (regionContains(@event.ResidenceAddressRegionKladrCode))
                    region = regions[@event.ResidenceAddressRegionKladrCode];
                else if (regionContains(@event.RegistrationAddressRegionKladrCode))
                    region = regions[@event.RegistrationAddressRegion];
                else
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.NotApplicable, @event.CreditApplicationId, $"Регион регистрации клиента не поддерживается {@event.RegistrationAddressRegion} ({@event.RegistrationAddressRegionKladrCode}), {@event.ResidenceAddressRegion} ({@event.ResidenceAddressRegionKladrCode})");
                    logger.LogInformation($"CheckCreditApplication. Skip. Unsupported region {@event.RegistrationAddressRegion} ({@event.RegistrationAddressRegionKladrCode}), {@event.ResidenceAddressRegion} ({@event.ResidenceAddressRegionKladrCode}). Duration: {beginTime.GetDuration()} Event: {@event}, Command: {command}");
                    return ProcessingResult.Ok();
                }
              
                var request = createRequest(@event.CreditApplicationId, @event, region);
                var response = await client.CreateCreditApplication(request);

                await repository.SaveCreditApplicationRequest(new CreditApplicationRequestModel
                {
                    CreditApplicationId = @event.CreditApplicationId,
                    CreatedDate = beginTime,
                    HttpStatusCode = response.StatusCode,
                    ErrorMessage = response.ErrorMessage,
                    SovcombankStatus = response.Content?.status,
                    SovcombankId = response.Content?.id,
                    SovcombankRandom = response.Content?.random,
                    SovcombankReason = response.Content?.reason?.ToString()
                });
                
                if (response.IsSuccessStatusCode)
                {
                    // ReSharper disable once PossibleNullReferenceException
                    if (response.Content?.status == "ok")
                    {
                        var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.Confirmed, @event.CreditApplicationId);
                        logger.LogInformation($"CheckCreditApplication. Request approved. Duration: {beginTime.GetDuration()} Request: {request}, Response: {response.Content}, Command: {command}");
                        return ProcessingResult.Ok();
                    }
                    else
                    {
                        var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ValidationErrorConfirm, @event.CreditApplicationId, response.Content.ToJsonString());
                        logger.LogInformation($"CheckCreditApplication. Request validation fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, Response: {response.Content}, Command: {command}");
                        return ProcessingResult.Ok();    
                    }
                }

                if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ValidationErrorConfirm, @event.CreditApplicationId, response.ErrorMessage);
                    logger.LogInformation($"CheckCreditApplication. Request validation fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.AuthorizaionErrorConfirm, @event.CreditApplicationId, response.ErrorMessage);
                    logger.LogError($"CheckCreditApplication. Request authorization fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                if (@event.RetryCount == messagingConfiguration.MaxRetryCount - 1)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, @event.CreditApplicationId,
                        $"Количество отправленных запросов: {messagingConfiguration.MaxRetryCount}. Последняя ошибка: {response.ErrorMessage}");
                    logger.LogWarning($"CheckCreditApplication. Request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Max retry count ({messagingConfiguration.MaxRetryCount}) exceeded, finish processing. Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                // Переповтор
                logger.LogWarning($"CheckCreditApplication. Request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}), try again later. Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}");
                return ProcessingResult.Fail();
            }
            catch (Exception e)
            {
                if (@event.RetryCount == messagingConfiguration.MaxRetryCount - 1)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, @event.CreditApplicationId,
                        $"Количество попыток обработки: {messagingConfiguration.MaxRetryCount}. {SERVER_UNEXPECTED_ERROR_DETAILS}");
                    logger.LogError(e, $"CheckCreditApplication. Error while processing request. Max retry count ({messagingConfiguration.MaxRetryCount}) exceeded, finish processing. Duration: {beginTime.GetDuration()} Event: {@event}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                logger.LogError(e, $"CheckCreditApplication. Error while processing request, try again later. Duration: {beginTime.GetDuration()} Event: {@event}");
                return ProcessingResult.Fail();
            }
        }

        public async Task<Response<CreditApplicationResponse>> ConfirmCreditApplication(Guid creditApplicationId, CreditApplicationRequest model)
        {
            var beginTime = DateTime.Now;
            try
            {
                string region;
                if (regionContains(model.ResidenceAddressRegionKladrCode))
                    region = regions[model.ResidenceAddressRegionKladrCode];
                else if (regionContains(model.RegistrationAddressRegionKladrCode))
                    region = regions[model.RegistrationAddressRegion];
                else
                {
                    logger.LogInformation($"CheckCreditApplication. Skip. Unsupported region {model.RegistrationAddressRegion} ({model.RegistrationAddressRegionKladrCode}), {model.ResidenceAddressRegion} ({model.ResidenceAddressRegionKladrCode}). Duration: {beginTime.GetDuration()} Model: {model}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.NotApplicable, $"Регион регистрации клиента не поддерживается {model.RegistrationAddressRegion} ({model.RegistrationAddressRegionKladrCode}), {model.ResidenceAddressRegion} ({model.ResidenceAddressRegionKladrCode})");
                }
                
                var request = createRequest(creditApplicationId, model, region);
                var response = await client.CreateCreditApplication(request);

                await repository.SaveCreditApplicationRequest(new CreditApplicationRequestModel
                {
                    CreditApplicationId = creditApplicationId,
                    CreatedDate = beginTime,
                    HttpStatusCode = response.StatusCode,
                    ErrorMessage = response.ErrorMessage,
                    SovcombankStatus = response.Content?.status,
                    SovcombankId = response.Content?.id,
                    SovcombankRandom = response.Content?.random,
                    SovcombankReason = response.Content?.reason?.ToString()
                });

                if (response.IsSuccessStatusCode)
                {
                    // ReSharper disable once PossibleNullReferenceException
                    if (response.Content?.status == "ok")
                    {
                        logger.LogInformation($"ConfirmCreditApplication. Request confirmed. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request.ToJsonString()}, Response: {response.Content}");
                        return createResponse(beginTime, CreditOrganizationRequestStatus.Confirmed);
                    }

                    logger.LogInformation($"ConfirmCreditApplication. Request rejected. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request.ToJsonString()}, Response: {response.Content}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.Rejected, response.Content.ToJsonString());
                }

                if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    logger.LogInformation($"ConfirmCreditApplication. Request validation fail (StatusCode = {response.StatusCode} {(int) response.StatusCode}). Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request.ToJsonString()}, ErrorMessage: {response.ErrorMessage}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.ValidationErrorConfirm, response.ErrorMessage);
                }

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    logger.LogError($"ConfirmCreditApplication. Request authorization fail (StatusCode = {response.StatusCode} {(int) response.StatusCode}). Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request.ToJsonString()}, ErrorMessage: {response.ErrorMessage}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.AuthorizaionErrorConfirm, response.ErrorMessage);
                }

                logger.LogWarning($"ConfirmCreditApplication. Request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request.ToJsonString()}, ErrorMessage: {response.ErrorMessage}");
                return createResponse(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, response.ErrorMessage);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"ConfirmCreditApplication. Error while processing request. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Model: {model}");
                return createResponse(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, SERVER_UNEXPECTED_ERROR_DETAILS);
            }
        }

        private Dictionary<string, string> createRequest(Guid creditApplicationId, CreditApplicationRequest request, string region)
        {
            var result = new Dictionary<string, string>();
            result.Add("api_key", configuration.ApiKey);
            result.Add("part_id", configuration.PartId.ToString());
            result.Add("product_name", "CreditDoctor");
            result.Add("utm_medium", "Agent");
            result.Add("utm_source", "YesFS");
            result.Add("utm_campaign", "credit_history");
            result.Add("utm_term", YESFS_URL);
            result.Add("utm_content", creditApplicationId.ToString());//todo
            result.Add("lastName", request.LastName);
            result.Add("firstName", request.FirstName);
            result.Add("parentName", request.MiddleName);
            result.Add("birthDay", request.DateOfBirth?.Day.ToString());
            result.Add("birthMonth", request.DateOfBirth?.Month.ToString());
            result.Add("birthYear", request.DateOfBirth?.Year.ToString());
            result.Add("region", region);
            result.Add("mainPhone", request.PhoneNumber);
            result.Add("secondaryPhone", request.AdditionalPhoneNumber);
            result.Add("email", request.Email);
            result.Add("personal_data_processing_permission_status", "Клиент дал согласие на передачу, обработку и хранение своих персональных данных");
            result.Add("personal_data_agent", YESFS_URL);
            result.Add("personal_data_user", "ПАО Совкомбанк");
            result.Add("personal_data_grant_date", request.PersonalDataProcessApproveDate.ToSovcombankDateFormat());
            
            return result;
        }

        private CreditApplicationDecisionsCommand publishDecisionCommand(DateTime date, CreditOrganizationRequestStatus status, Guid creditApplicationId, string details = null)
        {
            var command = new CreditApplicationDecisionsCommand
            {
                Date = date,
                CreditApplicationId = creditApplicationId,
                CreditApplications = new List<CreditApplicationStatusModel>
                {
                    new CreditApplicationStatusModel
                    {
                        CreditOrganizationId = configuration.CreditOrganizationId,
                        Status = status,
                        Details = details
                    }
                }
            };
            commandSender.SendCommand(command, BoundedContexts.CREDIT_APPLICATION_API, Routes.Decisions);
            return command;
        }
        
        private Response<CreditApplicationResponse> createResponse(DateTime date, CreditOrganizationRequestStatus status, string details = null)
        {
            return Response<CreditApplicationResponse>.Ok(new CreditApplicationResponse
            {
                Date = date,
                CreditApplications = new List<CreditApplicationStatusModel>
                {
                    new CreditApplicationStatusModel
                    {
                        CreditOrganizationId = configuration.CreditOrganizationId,
                        Status = status,
                        Details = details
                    }
                }
            });
        }

        private bool regionContains(string regionKladrCode)
        {
            return !string.IsNullOrWhiteSpace(regionKladrCode) && regions.ContainsKey(regionKladrCode);
        }
    }
}