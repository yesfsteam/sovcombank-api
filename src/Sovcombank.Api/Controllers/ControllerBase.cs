﻿using Microsoft.AspNetCore.Mvc;
using Yes.Infrastructure.Http;

namespace Sovcombank.Api.Controllers
{
    [ApiController]
    [Produces("application/json")]
    public class ControllerBase: Controller
    {
        protected IActionResult MakeResponse<T>(Response<T> result)
        {
            if (!result.IsSuccessStatusCode)
            {
                return StatusCode((int)result.StatusCode, result.ErrorMessage);
            }

            return Ok(result.Content);
        }
    }
}