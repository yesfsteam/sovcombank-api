﻿using System;

namespace Sovcombank.Api.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToSovcombankDateFormat(this DateTime date)
        {
            return date.ToString("yyyy-MM-dd");
        }
    }
}