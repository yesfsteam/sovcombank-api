﻿using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using Sovcombank.Api.Models;

namespace Sovcombank.Api.Data
{
    public interface ICreditApplicationRepository
    {
        Task SaveCreditApplicationRequest(CreditApplicationRequestModel model);
    }

    public class CreditApplicationRepository : ICreditApplicationRepository
    {
        private readonly IConfiguration configuration;

        public CreditApplicationRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public async Task SaveCreditApplicationRequest(CreditApplicationRequestModel model)
        {
            var queryParams = new
            {
                model.CreditApplicationId,
                model.CreatedDate,
                model.HttpStatusCode,
                model.ErrorMessage,
                model.SovcombankStatus,
                model.SovcombankId,
                model.SovcombankRandom,
                model.SovcombankReason
            };

            await using var connection = createConnection();
            await connection.ExecuteAsync(@"
                INSERT INTO credit_application_request (
                    credit_application_id,
                    created_date,
                    http_status_code,
                    error_message,
                    sovcombank_status,
                    sovcombank_id,
                    sovcombank_random,
                    sovcombank_reason
                ) VALUES (
                    :creditApplicationId::uuid,
                    :createdDate,
                    :httpStatusCode,
                    :errorMessage,
                    :sovcombankStatus,
                    :sovcombankId,
                    :sovcombankRandom,
                    :sovcombankReason
                );", queryParams);
        }

        private NpgsqlConnection createConnection()
        {
            return new NpgsqlConnection(configuration.GetConnectionString("Database"));
        }
    }
}